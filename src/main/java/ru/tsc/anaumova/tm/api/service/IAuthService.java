package ru.tsc.anaumova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.enumerated.Role;
import ru.tsc.anaumova.tm.model.User;

public interface IAuthService {

    void registry(@NotNull String login, @NotNull String password, @Nullable String email);

    void login(@NotNull String login, @NotNull String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role[] roles);

}