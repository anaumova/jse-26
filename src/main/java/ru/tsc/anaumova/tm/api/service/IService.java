package ru.tsc.anaumova.tm.api.service;

import ru.tsc.anaumova.tm.api.repository.IRepository;
import ru.tsc.anaumova.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}